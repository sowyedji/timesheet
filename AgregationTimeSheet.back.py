import xlrd
from xlrd import XLRDError
import os 
import sys
import glob
import csv
import re
import pandas

"Fichiers TS *.xlsm:toutes les ressources"
def listerXlsm(path):
    f_list = []
    files = [f for f in glob.glob(path + "**/*.xlsm", recursive = True)]
    for f in files:
        f_list.append(f)
    return f_list
def sommeHeureActivity(f,type_activite):
    somm = 0
    wb = xlrd.open_workbook(f)
    sh = wb.sheet_by_name(type_activite)
    #colonne1 = sh.col_values(0)                                        
    colonne2 = sh.col_values(1)
    i=1
    for i in range(1,7):
        somm = somm + float(colonne2[i])
    return somm
"Nombre de jours de congés "
def nombreJoursFerie(f):
    wb = xlrd.open_workbook(f)
    sh = wb.sheet_by_index(0)
    #colonne1 = sh.col_values(0)
    colonne2 = sh.col_values(1)
    return colonne2[12]
" Dates de debut et de fin"
def dateDebuTimeSheet(f):
    wb = xlrd.open_workbook(f)
    sh = wb.sheet_by_index(0)
    #colonne1 = sh.col_values(0)
    colonne2 = sh.col_values(1)
    return colonne2[9]
"La fonction de la ressource"
def dateFinTimeSheet(f):
    wb = xlrd.open_workbook(f)
    sh = wb.sheet_by_index(0)
    #colonne1 = sh.col_values(0)
    colonne2 = sh.col_values(1)
    return colonne2[10]
def fonctionRessource(f):
    wb = xlrd.open_workbook(f)
    sh = wb.sheet_by_index(0)
    #colonne1 = sh.col_values(0)
    colonne2 = sh.col_values(1)
    return colonne2[7]
"Heures d'absences de chaque TS"
def nombreHeuresAbsence(f):
    wb = xlrd.open_workbook(f)
    sh = wb.sheet_by_index(0)
    colonne4 = sh.col_values(3)
    i = 0
    for i in range (0,len(colonne4)):
        prog = re.compile(r'Heures Absence')
        res = prog.search(colonne4[i])
        if res:
            return colonne4[i+1]
        else:
            pass
    return colonne4[i+1]
"Nombre d'heures total"
def nombreHeuresTotal(f):
    wb = xlrd.open_workbook(f)
    sh = wb.sheet_by_index(0)
    colonne5 = sh.col_values(4)
    i = 0
    for i in range (0,len(colonne5)):
        prog = re.compile(r'Total')
        res = prog.search(colonne5[i])
        if res:
            return colonne5[i+1]
        else:
            pass
    return colonne5[i+1]
"Taux occupation "
def tauxOcuppation(f):
    wb = xlrd.open_workbook(f)
    sh = wb.sheet_by_index(0)
    colonne6 = sh.col_values(5)
    i = 0
    for i in range (0,len(colonne6)):
        prog = re.compile(r'Taux')
        res = prog.search(str(colonne6[i]))
        if res:
            return colonne6[i+1]
        else:
            pass
    return colonne6[i+1]

"Agr. des demandes de tous les TS"
def agregate(rep,fileDemande):
    f_list = listerXlsm(rep)
    for f in f_list:
        try:
            df = pandas.read_excel(f,0,header = 16,usecols = [3,4])
        except xlrd.XLRDError as er:
            print(os.path.abspath(f))
            print (er)
            #sys.exit(-1)
            
        matrice_sa = pandas.DataFrame(df).dropna().head(-2)
        matrice_sa.to_csv(rep+'/output/'+fileDemande,index = False,header = False,quoting = csv.QUOTE_NONE,encoding = 'latin1',sep = ";",mode = 'a')
"Main methode pour les tests"
if __name__ == "__main__":
    "initilisation des données"
    rep = input ("Enter le chemin  du répertoire contenant les TS :")
    fileReporting = rep+'/output/Agregation.csv'
    fileDemande = 'Demandes BU Managers.csv'
    labelNbreHav = "Nombre d'heures - Avant Vente"
    labelNbreHavd = "Nombre d'heures - Avant Vente D"
    labelNbreHpr = "Nombre d'heures - Projet"
    labelNbreHspc = "Nombre d'heures - Support clien"
    labelNbreHspi = "Nombre d'heures - Support inter"
    labelNbreHfr = "Nombre d'heures - Formation"
    labelNbreHmt = "Nombre d'heures - Montée en Com"
    labelNbreHaut = "Nombre d'heures - Autres"
    agregate(rep,fileDemande)
    f_list = listerXlsm(rep)
    for f in f_list:
        filepath = os.path.basename(f).split()
        av = str(sommeHeureActivity(f,labelNbreHav)).replace('.',',')
        avd = str(sommeHeureActivity(f,labelNbreHavd)).replace('.',',')
        pr = str(sommeHeureActivity(f,labelNbreHpr)).replace('.',',')
        spc = str(sommeHeureActivity(f,labelNbreHspc)).replace('.',',')
        spi = str(sommeHeureActivity(f,labelNbreHspi)).replace('.',',')
        fr = str(sommeHeureActivity(f,labelNbreHfr)).replace('.',',')
        mt = str(sommeHeureActivity(f,labelNbreHmt)).replace('.',',')
        aut = str(sommeHeureActivity(f,labelNbreHaut)).replace('.',',')
        nbrejoursf = nombreJoursFerie(f)
        #nobreJoursfe = float(nbrejoursf)*8
        nbreHrAbsence = str(nombreHeuresAbsence(f)).replace('.',',')
        tauxOcc = str(tauxOcuppation(f)).replace('.',',')
        semaineTimesheet = filepath[2][0:3]
        nbreHrTotal = str(nombreHeuresTotal(f)).replace('.',',')
        nobreJoursfestr = str(nbrejoursf).replace('.',',')
        dateDebuTimeSh = str(dateDebuTimeSheet(f)).replace('.',',')
        #print (f)
        with open(fileReporting,'a',newline='') as csfile:
            writer = csv.writer(csfile,delimiter = ';',quotechar = '"',quoting = csv.QUOTE_MINIMAL)
            writer.writerow([filepath[0],filepath[1],fonctionRessource(f),dateDebuTimeSh,dateFinTimeSheet(f),semaineTimesheet,av,avd,pr,spc,spi,fr,mt,aut,nobreJoursfestr,nbreHrAbsence,nbreHrTotal,tauxOcc])
            #Suite


        


        